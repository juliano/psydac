import h5py
from psydac.api.postprocessing import OutputManager
from psydac.linalg.block import BlockVector
from psydac.fem.basic import FemField
import numpy as np

def get_Om(V2h, comm, save_field_in=None):

    if save_field_in:
        Om = OutputManager(f'spaces.yml', save_field_in, comm=comm)
    else:
        Om = OutputManager(f'spaces.yml', f'fields.h5', comm=comm)
    Om.add_spaces(V2=V2h)
    if not save_field_in:
        Om.export_space_info()
    Om.close()

    return Om

def add_snapshot(Om, b, V, t, ts):
    
    B = FemField(V, b)
    Om.add_snapshot(t=t, ts=ts)
    Om.export_fields(u2=B)

def get_np_data(filename, step):

    with h5py.File(filename, "r") as f:

        snapshot = list(f.keys())[step]

        print(f[snapshot].attrs['t'])

        data_0 = f[snapshot]['C']['V2[0]']['u2[0]'][()]
        data_1 = f[snapshot]['C']['V2[1]']['u2[1]'][()]
        data_2 = f[snapshot]['C']['V2[2]']['u2[2]'][()]

        data = np.array([data_0, data_1, data_2])

        return data
    
def get_coefficients(V, data):

    V0 = V.spaces[0]
    V1 = V.spaces[1]
    V2 = V.spaces[2]

    v0 = V0.zeros()
    v1 = V1.zeros()
    v2 = V2.zeros()

    data0 = data[0]
    data1 = data[1]
    data2 = data[2]

    npts0 = V0.npts
    for n1 in range(npts0[0]):
        for n2 in range(npts0[1]):
            for n3 in range(npts0[2]):
                v0[n1, n2, n3] = data0[n1, n2, n3]

    npts1 = V1.npts
    for n1 in range(npts1[0]):
        for n2 in range(npts1[1]):
            for n3 in range(npts1[2]):
                v1[n1, n2, n3] = data1[n1, n2, n3]

    npts2 = V2.npts
    for n1 in range(npts2[0]):
        for n2 in range(npts2[1]):
            for n3 in range(npts2[2]):
                v2[n1, n2, n3] = data2[n1, n2, n3]

    data_vector = BlockVector(V, (v0, v1, v2))

    return data_vector
