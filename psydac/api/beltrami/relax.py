import  h5py

from sympde.topology import Cube
from sympde.topology import Derham

from psydac.api.beltrami.beltrami           import Beltrami
from psydac.api.beltrami.initial_condition  import get_A_fun
from psydac.api.discretization              import discretize
from psydac.api.beltrami.output             import get_np_data, get_coefficients

def relax(maxtime, ncells, degree, save_b_field_at, N_min, hamiltonian_at):
    """
    Relaxes a given initial condition (the curl of A from get_A_fun) in the unit Cube until either t=maxtime or step=maxsteps is reached.

    Parameters
    ----------
    maxtime : float
        Positive. Used to stop the code if either maxtime or maxsteps is reached.

    maxsteps : int
        Positive. Used to stop the code if either maxtime or maxsteps is reached.

    ncells : list | tuple of ints
        The amount of B-spline cells in each spatial direction.

    degree : list | tuple of ints
        The B-spline degree in each spatial direction

    beltrami_class : string
        Currently either 'kron', 'mpi' or 'output'. Determines which Beltrami class is used. Eventually, there should only be one.

    get_callable : bool
        If True, returns a callable function of the final "relaxed" magnetic field that can be used to visualize fieldlines and get a Poincarè plot.
        Only useful if relax(...) is called as a subroutine in PoincarePlot.py.
        Intermediate solution, as I don't have an output file yet saving the BlockVector coefficients of the "relaxed" state.

    poincare : bool
        If True, assumes relax(...) is called from inside /PoincarePlot/src. Changes the path of the .txt output file accordingly.
    
    """

    logical_domain = Cube('C', bounds1=(0, 1), bounds2=(0, 1), bounds3=(0, 1)) # mapping not included
    periodic = [True, True, True] # by choice of initial condition

    beltrami = Beltrami(logical_domain, ncells, degree, periodic, maxtime, save_b_field_at, hamiltonian_at, N_min)

    # given A = get_A_fun(), compute the discrete initial condition b \in V_h^2
    P1 = beltrami.P1
    C = beltrami.C

    A = get_A_fun()
    a = P1(A).coeffs
    b = C @ a

    beltrami.integrate(b)

def restart_relax(restart_field, restart_diagnostics, maxtime, ncells, degree, save_b_field_at, save_field_in, N_min, hamiltonian_at):

    logical_domain = Cube('C', bounds1=(0, 1), bounds2=(0, 1), bounds3=(0, 1))
    periodic = [True, True, True]

    derham_h = get_derham_h(logical_domain, ncells, degree, periodic)

    b = get_b_field(restart_field, derham_h)

    beltrami = Beltrami(logical_domain, ncells, degree, periodic, maxtime, save_b_field_at, hamiltonian_at, N_min)

    beltrami.integrate(b, restart=True, save_field_in=save_field_in, restart_diagnostics=restart_diagnostics)

def get_b_field(filename, derham_h):

    with h5py.File(filename, "r") as f:

        snaps = len(list(f.keys()))-1

    data = get_np_data(filename, snaps)
    coeffs = get_coefficients(derham_h.V2.vector_space, data)

    return coeffs

def get_derham_h(domain, ncells, degree, periodic):

    derham = Derham(domain)

    domain_h = discretize(domain, ncells=ncells, periodic=periodic)
    derham_h = discretize(derham, domain_h, degree=degree)

    return derham_h
