import  time
import  h5py
import  numpy                as np
import  matplotlib.pyplot    as plt
#from mpi4py                  import MPI

from sympde.topology import Cube
from sympde.topology import Derham
from sympde.topology import elements_of
from sympde.expr     import BilinearForm, integral
from sympde.calculus import dot

from psydac.api.discretization      import discretize
from psydac.api.settings            import PSYDAC_BACKEND_GPYCCEL
from psydac.api.beltrami.output     import get_np_data, get_coefficients

def plot(file_path_as_str=None, label=None, snapshots=None):

    if file_path_as_str:
        f = open(file_path_as_str, 'r')
    else:
        f = open('diagnostics/diagnostics.txt', 'r')

    lines = f.readlines()

    t_column_number = 1
    h_column_number = 3
    s_column_number = 4
    d_column_number = 5
    j_column_number = 6

    result_t = []
    result_h = []
    result_s = []
    result_d = []
    result_j = []

    for x in lines:
        result_t.append(x.split()[t_column_number])
        result_h.append(x.split()[h_column_number])
        result_s.append(x.split()[s_column_number])
        result_d.append(x.split()[d_column_number])
        result_j.append(x.split()[j_column_number])

    f.close()

    length = len(result_t)-1
    fin_t = float(result_t[-1])

    fig, axs = plt.subplots(2, 2, figsize=(24, 8))

    axs[0, 0].set_xlim([0, fin_t])
    axs[0, 0].set_xlabel('t')
    axs[0, 1].set_xlim([0, fin_t])
    axs[0, 1].set_xlabel('t')
    axs[1, 0].set_xlim([0, fin_t])
    axs[1, 0].set_xlabel('t')
    axs[1, 1].set_xlim([0, fin_t])
    axs[1, 1].set_xlabel('t')

    axs[0, 0].set_ylim([1e-17, 1e-10])
    axs[0, 0].set_yscale('log')
    axs[0, 1].set_ylim([6.25, 6.6])
    axs[1, 0].set_ylim([1e-13, 1e-8])
    axs[1, 0].set_yscale('log')
    axs[1, 1].set_ylim([0, 2500])

    axs[0, 0].set_title('| H(B_t) - H(B_0) | / H(B_0)')
    axs[0, 1].set_title('S(B_t)')
    axs[1, 0].set_title('|| div(B_t) ||')
    axs[1, 1].set_title('|| J x H ||')

    t = [float(result_t[i+1]) for i in range(length)]
    t = np.array(t)
    tj = t[1:len(t)]
    h = []
    th = []
    for i in range(length):
        hi = result_h[i+1]
        if hi != 'x':
            h.append(float(hi))
            th.append(t[i])
    #h = [float(result_h[i+1]) for i in range(length)]
    s = [float(result_s[i+1]) for i in range(length)]
    d = [float(result_d[i+1]) for i in range(length)]
    j = [float(result_j[i+2]) for i in range(length-1)]

    h0 = h[0]
    rel_h_err = [abs(h0-h[i])/h0 for i in range(len(h))]

    if label:
        axs[0, 0].plot(th, rel_h_err, '^', color='c', label=label)
        axs[0, 1].plot(t, s, color='c', label=label)
        axs[1, 0].plot(t, d, color='c', label=label)
        axs[1, 1].plot(tj, j, color='c', label=label)

        if snapshots:
            ncells = [32, 32, 32]
            degree = [2, 2, 2]
            periodic = [True, True, True]
            for shots in snapshots:
                Ts, Ds, Es = add_snapshots(shots, ncells, degree, periodic)
                l = len(Ts)

                for n in range(l):
                    axs[1, 0].plot(Ts[n], Ds[n], '^')
                    axs[0, 1].plot(Ts[n], Es[n], '^')

        axs[0, 0].legend()
        axs[0, 1].legend()
        axs[1, 0].legend()
        axs[1, 1].legend()
    else:
        axs[0, 0].plot(th, rel_h_err, '^', color='c')
        axs[0, 1].plot(t, s, color='c')
        axs[1, 0].plot(t, d, color='c')
        axs[1, 1].plot(tj, j, color='c')

        if snapshots:
            ncells = [32, 32, 32]
            degree = [2, 2, 2]
            periodic = [True, True, True]
            for shots in snapshots:
                Ts, Ds, Es = add_snapshots(shots, ncells, degree, periodic)
                l = len(Ts)

                for n in range(l):
                    axs[1, 0].plot(Ts[n], Ds[n], '^')
                    axs[0, 1].plot(Ts[n], Es[n], '^')

    plt.show()

def plot_compare(txt1, txt2, txt3, label1, label2, label3, tmax=None):

    assert label1 is not None
    assert label2 is not None

    f1 = open(txt1, 'r')
    f2 = open(txt2, 'r')

    lines1 = f1.readlines()
    lines2 = f2.readlines()

    t_column_number = 1
    h_column_number = 3
    s_column_number = 4
    d_column_number = 5
    j_column_number = 6

    result1_t = []
    result1_h = []
    result1_s = []
    result1_d = []
    result1_j = []

    result2_t = []
    result2_h = []
    result2_s = []
    result2_d = []
    result2_j = []

    for x in lines1:
        result1_t.append(x.split()[t_column_number])
        result1_h.append(x.split()[h_column_number])
        result1_s.append(x.split()[s_column_number])
        result1_d.append(x.split()[d_column_number])
        result1_j.append(x.split()[j_column_number])

    for x in lines2:
        result2_t.append(x.split()[t_column_number])
        result2_h.append(x.split()[h_column_number])
        result2_s.append(x.split()[s_column_number])
        result2_d.append(x.split()[d_column_number])
        result2_j.append(x.split()[j_column_number])

    f1.close()
    f2.close()

    if txt3:
        assert label3 is not None

        f3 = open(txt3, 'r')
        lines3 = f3.readlines()
        result3_t = []
        result3_h = []
        result3_s = []
        result3_d = []
        result3_j = []

        for x in lines3:
            result3_t.append(x.split()[t_column_number])
            result3_h.append(x.split()[h_column_number])
            result3_s.append(x.split()[s_column_number])
            result3_d.append(x.split()[d_column_number])
            result3_j.append(x.split()[j_column_number])

        f3.close()

    if not tmax:
        if not txt3:
            t1max = float(result1_t[-1])
            t2max = float(result2_t[-1])
            tmax = min(t1max, t2max)
        else:
            t1max = float(result1_t[-1])
            t2max = float(result2_t[-1])
            t3max = float(result3_t[-1])
            tmax = min(t1max, t2max, t3max)
    t1 = []
    n1 = 1
    l1 = len(result1_t) - 1
    while (n1 <= l1) and (float(result1_t[n1]) <= tmax):
        t1.append(float(result1_t[n1]))
        n1 += 1
    length1 = len(t1)
    t1 = np.array(t1)

    h1 = []
    th1 = []
    for i in range(length1):
        hi1 = result1_h[i+1]
        if hi1 != 'x':
            h1.append(float(hi1))
            th1.append(t1[i])
    #h1 = [float(result1_h[i+1]) for i in range(length1)]
    s1 = [float(result1_s[i+1]) for i in range(length1)]
    d1 = [float(result1_d[i+1]) for i in range(length1)]
    j1 = [float(result1_j[i+2]) for i in range(length1-1)]
    t1j = t1[1:length1]

    t2 = []
    n2 = 1
    l2 = len(result2_t) - 1
    while (n2 <= l2) and (float(result2_t[n2]) <= tmax):
        t2.append(float(result2_t[n2]))
        n2 += 1
    length2 = len(t2)

    h2 = []
    th2 = []
    for i in range(length2):
        hi2 = result2_h[i+1]
        if hi2 != 'x':
            h2.append(float(hi2))
            th2.append(t2[i])
    #h2 = [float(result2_h[i+1]) for i in range(length2)]
    s2 = [float(result2_s[i+1]) for i in range(length2)]
    d2 = [float(result2_d[i+1]) for i in range(length2)]
    j2 = [float(result2_j[i+2]) for i in range(length2-1)]
    t2j = t2[1:length2]

    if txt3:
        t3 = []
        n3 = 1
        l3 = len(result3_t) - 1
        while (n3 <= l3) and (float(result3_t[n3]) <= tmax):
            t3.append(float(result3_t[n3]))
            n3 += 1
        length3 = len(t3)

        h3 = []
        th3 = []
        for i in range(length3):
            hi3 = result3_h[i+1]
            if hi3 != 'x':
                h3.append(float(hi3))
                th3.append(t3[i])
        #h3 = [float(result3_h[i+1]) for i in range(length3)]
        s3 = [float(result3_s[i+1]) for i in range(length3)]
        d3 = [float(result3_d[i+1]) for i in range(length3)]
        j3 = [float(result3_j[i+2]) for i in range(length3-1)]
        t3j = t3[1:length3]

    fig, axs = plt.subplots(2, 2, figsize=(24, 8))

    h10 = h1[0]
    rel_h1_err = [abs(h10-h1[i])/h10 for i in range(len(h1))]
    h20 = h2[0]
    rel_h2_err = [abs(h20-h2[i])/h20 for i in range(len(h2))]
    if txt3:
        h30 = h3[0]
        rel_h3_err = [abs(h30-h3[i])/h30 for i in range(len(h3))]

    axs[0, 0].set_xlim([0, tmax])
    axs[0, 0].set_xlabel('t')
    axs[0, 1].set_xlim([0, tmax])
    axs[0, 1].set_xlabel('t')
    axs[1, 0].set_xlim([0, tmax])
    axs[1, 0].set_xlabel('t')
    axs[1, 1].set_xlim([0, tmax])
    axs[1, 1].set_xlabel('t')

    axs[0, 0].set_ylim([1e-17, 1e-10])
    axs[0, 0].set_yscale('log')
    axs[0, 1].set_ylim([6.25, 6.6])
    axs[1, 0].set_ylim([1e-13, 1e-8])
    axs[1, 0].set_yscale('log')
    axs[1, 1].set_ylim([0, 2500])

    axs[0, 0].set_title('| H(B_t) - H(B_0) | / H(B_0)')
    axs[0, 1].set_title('S(B_t)')
    axs[1, 0].set_title('|| div(B_t) ||')
    axs[1, 1].set_title('|| J x H ||')

    axs[0, 0].plot(th1, rel_h1_err, '^', label=label1)
    axs[0, 0].plot(th2, rel_h2_err, '^', label=label2)
    if txt3:
        axs[0, 0].plot(th3, rel_h3_err, '^', label=label3)
    axs[0, 0].legend()

    axs[0, 1].plot(t1, s1, label=label1)
    axs[0, 1].plot(t2, s2, label=label2)
    if txt3:
        axs[0, 1].plot(t3, s3, label=label3)
    axs[0, 1].legend()

    axs[1, 0].plot(t1, d1, label=label1)
    axs[1, 0].plot(t2, d2, label=label2)
    if txt3:
        axs[1, 0].plot(t3, d3, label=label3)
    axs[1, 0].legend()

    axs[1, 1].plot(t1j, j1, label=label1)
    axs[1, 1].plot(t2j, j2, label=label2)
    if txt3:
        axs[1, 1].plot(t3j, j3, label=label3)
    axs[1, 1].legend()

    plt.show()

def add_snapshots(filename, ncells, degree, periodic):

    backend = PSYDAC_BACKEND_GPYCCEL
    #comm = MPI.COMM_WORLD

    domain = Cube('C', bounds1=(0, 1), bounds2=(0, 1), bounds3=(0, 1))
    derham = Derham(domain)

    domain_h = discretize(domain, ncells=ncells, periodic=periodic)#, comm=comm)
    derham_h = discretize(derham, domain_h, degree=degree)

    G, C, D = derham_h.derivatives_as_matrices

    u2, v2 = elements_of(derham.V2, names='u2, v2')
    u3, v3 = elements_of(derham.V3, names='u3, v3')

    a2 = BilinearForm((u2, v2), integral(domain, dot(u2, v2)))
    a3 = BilinearForm((u3, v3), integral(domain, u3 * v3))

    start = time.time()
    a2_h = discretize(a2, domain_h, (derham_h.V2, derham_h.V2), backend=backend)
    stop = time.time()
    print(f"discretize a2: {stop-start} seconds") 
    M2 = a2_h.assemble()

    start = time.time()
    a3_h = discretize(a3, domain_h, (derham_h.V3, derham_h.V3), backend=backend)
    stop = time.time()
    print(f"discretize a3: {stop-start} seconds") 
    M3 = a3_h.assemble()

    with h5py.File(filename, "r") as f:

        snaps = len(list(f.keys()))-1

    Ts = []
    Ds = []
    Es = []

    for n in range(snaps):
        data = get_np_data(filename, n+1)
        coeffs = get_coefficients(derham_h.V2.vector_space, data)
        divergence = _divergence(coeffs, D, M3)
        entropy = _entropy(coeffs, M2)
        with h5py.File(filename, "r") as f:
            snapshot = list(f.keys())[n+1]
            t = f[snapshot].attrs['t']

        Ts.append(t)
        Ds.append(divergence)
        Es.append(entropy)

    return Ts, Ds, Es

def parallel_data_transfer(b, v, V):

    localslice0 = tuple([slice(s, e+1) for s, e in zip(V[0].starts, V[0].ends)])
    v[0][localslice0] = b[0][localslice0]
    localslice1 = tuple([slice(s, e+1) for s, e in zip(V[1].starts, V[1].ends)])
    v[1][localslice1] = b[1][localslice1]
    localslice2 = tuple([slice(s, e+1) for s, e in zip(V[2].starts, V[2].ends)])
    v[2][localslice2] = b[2][localslice2]

    return v

def _divergence(b, D, M3):

    div = D @ b
    out = np.sqrt( ( M3 @ div ).dot(  div) )

    return out

def _entropy(b, M2):

    out = ( M2 @ b ).dot( b )
    out *= 0.5

    return out