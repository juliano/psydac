import numpy as np

from sympde.topology import Line
from sympde.topology import Derham
from sympde.topology import elements_of
from sympde.topology import Derham
from sympde.expr     import BilinearForm, integral

from scipy.sparse   import dia_matrix

from psydac.api.discretization      import discretize
from psydac.linalg.kron             import KroneckerLinearSolver
from psydac.linalg.direct_solvers   import BandedSolver
from psydac.linalg.block            import BlockLinearOperator

def to_bnd(A):

    dmat = dia_matrix(A.toarray(), dtype=A.dtype)
    la   = abs(dmat.offsets.min())
    ua   = dmat.offsets.max()
    cmat = dmat.tocsr()

    A_bnd = np.zeros((1+ua+2*la, cmat.shape[1]), A.dtype)

    for i,j in zip(*cmat.nonzero()):
        A_bnd[la+ua+i-j, j] = cmat[i,j]

    return A_bnd, la, ua

def matrix_to_bandsolver(A):
    A.remove_spurious_entries()
    A_bnd, la, ua = to_bnd(A)
    return BandedSolver(ua, la, A_bnd)

def get_M1_block_kron_solver(space, ncells, degree, periodic):

    assert len(ncells) == 3
    assert len(degree) == 3
    assert len(periodic) == 3

    n1 = ncells[0]
    n2 = ncells[1]
    n3 = ncells[2]
    p1 = degree[0]
    p2 = degree[1]
    p3 = degree[2]
    P1 = periodic[0]
    P2 = periodic[1]
    P3 = periodic[2]

    # for now
    assert (n1 == n2) and (n1 == n3)
    assert (p1 == p2) and (p1 == p3)
    assert (P1 == P2) and (P1 == P3)

    ncells_1d = [n1]
    degree_1d = [p1]
    periodic_1d = [P1]

    domain_1d = Line('L', bounds=(0,1))
    derham_1d = Derham(domain_1d)

    domain_1d_h = discretize(domain_1d, ncells=ncells_1d, periodic=periodic_1d)
    derham_1d_h = discretize(derham_1d, domain_1d_h, degree=degree_1d)

    u_1d_0, v_1d_0 = elements_of(derham_1d.V0, names='u_1d_0, v_1d_0')
    u_1d_1, v_1d_1 = elements_of(derham_1d.V1, names='u_1d_1, v_1d_1')

    a_1d_0 = BilinearForm((u_1d_0, v_1d_0), integral(domain_1d, u_1d_0 * v_1d_0))
    a_1d_1 = BilinearForm((u_1d_1, v_1d_1), integral(domain_1d, u_1d_1 * v_1d_1))

    a_1d_0_h = discretize(a_1d_0, domain_1d_h, (derham_1d_h.V0, derham_1d_h.V0))
    a_1d_1_h = discretize(a_1d_1, domain_1d_h, (derham_1d_h.V1, derham_1d_h.V1))

    M_1d_0 = a_1d_0_h.assemble()
    M_1d_1 = a_1d_1_h.assemble()

    V1 = space[0]
    V2 = space[1]
    V3 = space[2]

    B1_mat = [M_1d_1, M_1d_0, M_1d_0]
    B2_mat = [M_1d_0, M_1d_1, M_1d_0]
    B3_mat = [M_1d_0, M_1d_0, M_1d_1]

    B1_solvers = [matrix_to_bandsolver(Ai) for Ai in B1_mat]
    B2_solvers = [matrix_to_bandsolver(Ai) for Ai in B2_mat]
    B3_solvers = [matrix_to_bandsolver(Ai) for Ai in B3_mat]

    B1_kron_inv = KroneckerLinearSolver(V1, B1_solvers)
    B2_kron_inv = KroneckerLinearSolver(V2, B2_solvers)
    B3_kron_inv = KroneckerLinearSolver(V3, B3_solvers)

    #M1_block_kron_solver = BlockDiagonalSolver(space, (B1_kron_inv, B2_kron_inv, B3_kron_inv))
    M1_block_kron_solver = BlockLinearOperator(space, space, ((B1_kron_inv, None, None), 
                                                              (None, B2_kron_inv, None), 
                                                              (None, None, B3_kron_inv)))

    return M1_block_kron_solver
