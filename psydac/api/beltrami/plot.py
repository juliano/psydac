from psydac.api.beltrami.beltrami_plot import plot, plot_compare

compare = False
snapshots = ['data/Nm5/fields/fields_3e-4_Nm5.h5', 
             'data/Nm5/fields/fields_5e-4_Nm5.h5', 
             'data/Nm5/fields/fields_1e-3_Nm5.h5', 
             'data/Nm5/fields/fields_2e-3_Nm5.h5',
             'data/Nm5/fields/fields_3e-3_Nm5.h5']
#snapshots = None
onlytwo = False

txt1 = 'diagnostics/nc32_3e-3_Nm5_par_out.txt'
label1 = 'Nm5 - parallel - out'
txt2 = 'diagnostics/nc32_3e-4_Nm5_par_out.txt'
label2= 'Nm5 - parallel - out'
txt3 = 'diagnostics/nc32_3e-4_Nm10_par_out.txt'
label3 = 'Nm10 - parallel - out'

if compare:
    if onlytwo:
        plot_compare(txt1=txt1, txt2=txt2, txt3=None, label1=label1, label2=label2, label3=None)
    else:
        plot_compare(txt1, txt2, txt3, label1, label2, label3)
else:
    plot(txt1, label=label1, snapshots=snapshots)
