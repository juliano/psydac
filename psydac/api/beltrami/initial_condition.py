import numpy as np

from mpi4py import MPI

from sympde.calculus import dot, cross
from sympde.expr     import BilinearForm
from sympde.expr     import integral
from sympde.topology import Cube
from sympde.topology import Derham
from sympde.topology import NormalVector
from sympde.topology import elements_of

from psydac.api.discretization import discretize
from psydac.api.settings       import PSYDAC_BACKEND_GPYCCEL
from psydac.fem.basic          import FemField
from psydac.linalg.basic       import IdentityOperator
from psydac.linalg.solvers     import inverse

def get_A_fun(n=1, m=1, A0=1e04):
    """Get the tuple A = (A1, A2, A3), where each entry is a function taking x,y,z as input."""

    mu_tilde = np.sqrt(m**2 + n**2)  

    eta = lambda x, y, z: x**2 * (1-x)**2 * y**2 * (1-y)**2 * z**2 * (1-z)**2

    u1  = lambda x, y, z:  A0 * (n/mu_tilde) * np.sin(np.pi * m * x) * np.cos(np.pi * n * y)
    u2  = lambda x, y, z: -A0 * (m/mu_tilde) * np.cos(np.pi * m * x) * np.sin(np.pi * n * y)
    u3  = lambda x, y, z:  A0 * np.sin(np.pi * m * x) * np.sin(np.pi * n * y)

    A1 = lambda x, y, z: eta(x, y, z) * u1(x, y, z)
    A2 = lambda x, y, z: eta(x, y, z) * u2(x, y, z)
    A3 = lambda x, y, z: eta(x, y, z) * u3(x, y, z)

    A = (A1, A2, A3)
    return A

def discrete_vector_field(ncells, degree):

    domain = Cube('C', bounds1=(0, 1), bounds2=(0, 1), bounds3=(0, 1))
    periodic = [True, True, True]

    derham = Derham(domain)
    domain_h = discretize(domain, ncells=ncells, periodic=periodic, comm=MPI.COMM_WORLD)
    derham_h = discretize(derham, domain_h, degree=degree)
    G, C, D = derham_h.derivatives_as_matrices
    P0, P1, P2, P3  = derham_h.projectors()
    
    u1, v1 = elements_of(derham.V1, names='u1, v1')
    u2, v2 = elements_of(derham.V2, names='u2, v2')
    nn = NormalVector('nn')
    a1_bc = BilinearForm((u1, v1),
               integral(domain.boundary, 1e30 * dot(cross(u1, nn), cross(v1, nn))))
    a1_bc_h = discretize(a1_bc, domain_h, (derham_h.V1, derham_h.V1), backend=PSYDAC_BACKEND_GPYCCEL)
    M1_bc   = a1_bc_h.assemble()
    a2_bc = BilinearForm((u2, v2),
               integral(domain.boundary, 1e30 * dot(u2, nn) * dot(v2, nn)))
    a2_bc_h = discretize(a2_bc, domain_h, (derham_h.V2, derham_h.V2), backend=PSYDAC_BACKEND_GPYCCEL)
    M2_bc   = a2_bc_h.assemble()
    V1 = derham_h.V1.vector_space
    V2 = derham_h.V2.vector_space
    I1 = IdentityOperator(V1, V1)
    I2 = IdentityOperator(V2, V2)
    BC1 = I1 + M1_bc
    BC2 = I2 + M2_bc
    BC1_inverse = inverse(BC1, 'cg', tol=1e-12, maxiter=1000)
    BC2_inverse = inverse(BC2, 'cg', tol=1e-12, maxiter=1000)

    A = get_A_fun()
    a_temp = P1(A).coeffs
    a = BC1_inverse @ a_temp
    b = BC2_inverse @ C @ a

    B = FemField(derham_h.V2, b)
    def B_fun(x):
        y = B(x[0], x[1], x[2])
        return np.array([y[0], y[1], y[2]])

    return B_fun
