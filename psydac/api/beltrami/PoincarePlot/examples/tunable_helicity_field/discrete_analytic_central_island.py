"""
Configuration file for a Poincare' plot.

Test problem: Example of a vector field with tunable helicity on
the unit cube.
"""

import numpy as np

from psydac.api.beltrami.initial_condition import discrete_vector_field

# Output 
working_dir = '../examples/tunable_helicity_field/'
output_file = 'test_tunable_helicity_field'
save_fieldlines = True 
save_field_values = False

# Parameter for the Poincare plot
dimensionality = 3
number_of_fieldlines = 100
number_of_intersections = 1000
period = 1.0

# ... domain specification ...
# lower_bounds = (0.0, 0.0, 0.0)
# upper_bounds = (1.0, 1.0, 1.0)


# # ... initial points for the fieldlines ...
jmax = number_of_fieldlines
island1 = np.array([ [0.754611, 0.5, 0.252325] ])

initial_points = np.vstack((island1))


# Parameter for the ode integrator
max_time_steps = 1000000
dt = 0.001

relative_tolerance = 1.e-13
absolute_tolerance = 1.e-13
max_substeps = 1000

#############################################
ncells   = [32, 32, 32]                     #
degree   = [2, 2, 2]                        #
                                            #
B = discrete_vector_field(ncells, degree)   #
#############################################

def vector_field(x):
    return B(x)

# Surface for the Poincare section
def Poincare_section(x, y):
    
    """
    Given two points x and y, check if the segment x --> y intersects the desired
    surface for the Poincare section and

      No intersection detected  --> return None
      One intersection detected --> return coordinates (u,v) of the intersection

    In this case the section is simply a vertical section at x[1] = 0.5 parametrized
    by coordinate u = x[0] and v = x[2]
    
    This method to detect the Poincare' plane avoid dependence on the time step.
    
    """

    delta_x = x[1] - 0.5
    delta_y = y[1] - 0.5

    if delta_x*delta_y < 0:
        # ... evaluate intersection ...
        t0 = (0.5 - x[1]) / (y[1] - x[1])
        u = x[0] + t0 * (y[0] - x[0])
        v = x[2] + t0 * (y[2] - x[2])
        return [u,v]
    elif delta_x != 0 and delta_y == 0:
        u = y[0]
        v = y[2]
        return [u,v]
    else:
        return None
