"""
Definition of the field.
by C. Bressan
"""

import numpy as np


class TunableHelicityB(object):
    
    """
    Analytical model of divergence-free field B contructed on a cubic domain
    Omega = [0,1]^3 such that
       
       1)  div(B) = 0 in Omega;
       2)  B = 0 on the boundary of Omega (the whole vector vanishes);
       3)  helicity H is non-trivial and can be tuned by a choice of the parameters.

    Usage:
     
       >>> modelB = TunbleHelicityB(m=2, n=3, A0=1.0)
       >>> x = [0.5, 0.5, 0.5]
       >>> B_at_center = modelB.eval(x) # evaluate at the point x
       >>> x_grid = np.mgrid[0.:1.:.1, 0.:1.:.1, 0.:1.:.1]
       >>> B = modelB.elav(x_grid) # evaluate on a uniform grid
    """

    # Constructor
    def __init__(self, **kwargs):
        # Just store the input parameters
        self.parameters = kwargs
        return None

    # Function used to localize in the interior of Omega
    def _h(self, a):
        return a**2 * (1-a)**2
    
    # Derivative 
    def _dh(self, a):
        return 2.*a * (1-a)**2 - 2.*a**2 * (1-a)
        
    # Auxiliary vector field
    def _u(self, x):

        A0 = self.parameters['A0']
        m = self.parameters['m']
        n = self.parameters['n']

        mu_tilde = np.sqrt(m**2 + n**2)
        
        u0 =  A0 * (n/mu_tilde) * np.sin(np.pi*m*x[0])*np.cos(np.pi*n*x[1])
        u1 = -A0 * (m/mu_tilde) * np.cos(np.pi*m*x[0])*np.sin(np.pi*n*x[1])
        u2 =  A0 * np.sin(np.pi*m*x[0])*np.sin(np.pi*n*x[1])

        return np.array([u0, u1, u2])

    # Localizing function
    def _eta(self, x):
        return self._h(x[0])*self._h(x[1])*self._h(x[2])

    # Gradient of the localizing function eta
    def _grad_eta(self, x):

        grad_eta0 = self._dh(x[0])*self._h(x[1])*self._h(x[2])
        grad_eta1 = self._h(x[0])*self._dh(x[1])*self._h(x[2])
        grad_eta2 = self._h(x[0])*self._h(x[1])*self._dh(x[2])
        
        return np.array([grad_eta0, grad_eta1, grad_eta2])
    
    # The main function for the evaluation
    def eval(self, x):

        """
        Evaluate the model field at the point (or grid of points) x.
        """
        m = self.parameters['m']
        n = self.parameters['n']
        
        mu = np.pi * np.sqrt(m**2 + n**2)

        eta = self._eta(x)
        grad_eta = self._grad_eta(x)
        uu = self._u(x)

        B1 = np.cross(grad_eta, uu, axisa=0, axisb=0, axisc=0) 
        B2 = mu * np.array([eta * uu[i] for i in range(3)])
        
        return B1 + B2
