"""
Configuration file for a Poincare' plot.

Test problem: Example of a vector field with tunable helicity on
the unit cube.
"""

import h5py
import numpy as np

from sympde.topology import Cube
from sympde.topology import Derham

from psydac.fem.basic                       import FemField
from psydac.api.discretization              import discretize
from psydac.api.beltrami.output             import get_np_data, get_coefficients

# Output 
working_dir = '../examples/tunable_helicity_field/'
output_file = 'test_tunable_helicity_field'
save_fieldlines = True
save_field_values = True

# Parameter for the Poincare plot
dimensionality = 3
number_of_fieldlines = 1
number_of_intersections = 1000
period = 1.0

# ... domain specification ...
# lower_bounds = (0.0, 0.0, 0.0)
# upper_bounds = (1.0, 1.0, 1.0)


# # ... initial points for the fieldlines ...
jmax = number_of_fieldlines
island1 = np.array([ [0.754611, 0.5, 0.252325] ])

initial_points = np.vstack((island1))


# Parameter for the ode integrator
max_time_steps = 2500000
dt = 0.00025

relative_tolerance = 1.e-14
absolute_tolerance = 1.e-14
max_substeps = 2000

##############################################################################
def get_b_field(filename, derham_h):                                       ###

    #with h5py.File(filename, "r") as f:

    #   snaps = len(list(f.keys()))-1

    snaps = 6 # for 4,0e-2

    data = get_np_data(filename, snaps)
    coeffs = get_coefficients(derham_h.V2.vector_space, data)

    return coeffs
def get_derham_h(domain, ncells, degree, periodic):                        ###

    derham = Derham(domain)

    domain_h = discretize(domain, ncells=ncells, periodic=periodic)
    derham_h = discretize(derham, domain_h, degree=degree)

    return derham_h
logical_domain = Cube('C', bounds1=(0, 1), bounds2=(0, 1), bounds3=(0, 1)) ###
ncells = [32, 32, 32]                                                      ###
degree = [2, 2, 2]                                                         ###
periodic = [True, True, True]                                              ###
restart_field = '../../data/Nm5/fields/fields_1_1e-2_1e-1.h5'              ###
derham_h = get_derham_h(logical_domain, ncells, degree, periodic)          ###
b = get_b_field(restart_field, derham_h)                                   ###
B_field = FemField(derham_h.V2, b)                                         ###
def B(x):                                                                  ###
    y = B_field(x[0], x[1], x[2])
    return np.array([y[0], y[1], y[2]])
##############################################################################

vector_field = B

#def vector_field(x):
#    return B(x)

# Surface for the Poincare section
def Poincare_section(x, y):
    
    """
    Given two points x and y, check if the segment x --> y intersects the desired
    surface for the Poincare section and

      No intersection detected  --> return None
      One intersection detected --> return coordinates (u,v) of the intersection

    In this case the section is simply a vertical section at x[1] = 0.5 parametrized
    by coordinate u = x[0] and v = x[2]
    
    This method to detect the Poincare' plane avoid dependence on the time step.
    
    """

    delta_x = x[1] - 0.5
    delta_y = y[1] - 0.5

    if delta_x*delta_y < 0:
        # ... evaluate intersection ...
        t0 = (0.5 - x[1]) / (y[1] - x[1])
        u = x[0] + t0 * (y[0] - x[0])
        v = x[2] + t0 * (y[2] - x[2])
        return [u,v]
    elif delta_x != 0 and delta_y == 0:
        u = y[0]
        v = y[2]
        return [u,v]
    else:
        return None
