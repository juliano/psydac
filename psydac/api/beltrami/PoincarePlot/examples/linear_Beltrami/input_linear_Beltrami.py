"""
Configuration file for a Poincare' plot.

Test problem: Linear Beltrami field on the unit cube 
with periodic boundary conditions.
"""

import numpy as np

# Output 
working_dir = '../examples/linear_Beltrami/'
output_file = 'test_linear_Beltrami'
save_fieldlines = True 
save_field_values = False

# Parameter for the Poincare plot
dimensionality = 3
number_of_fieldlines = 20
number_of_intersections = 500
period = 1.0

# ... domain specification ...
## lower_bounds = (0.6, 0.49, 0.2)
## upper_bounds = (0.9, 0.51, 0.8)


# ... initial points for the fieldlines ...
jmax = number_of_fieldlines
initial_points = np.array([
    [0.6 + (j/jmax-1) * (0.9 - 0.6), 0.5, 0.5]
    for j in range(jmax)
])


# Parameter for the ode integrator
max_time_steps = 10000000
dt = 0.1

relative_tolerance = 1.e-13
absolute_tolerance = 1.e-13
max_substeps = 1000

                      
# Definition of the vector field
m = 1
n = 1
A0 = 1.0

def vector_field(x):

    """
    Evaluate the model field at the point (or grid of points) x.
    """
    mu_tilde = np.sqrt(m**2 + n**2)    
    
    u0 =  A0 * (n/mu_tilde) * np.sin(np.pi*m*x[0])*np.cos(np.pi*n*x[1])
    u1 = -A0 * (m/mu_tilde) * np.cos(np.pi*m*x[0])*np.sin(np.pi*n*x[1])
    u2 =  A0 * np.sin(np.pi*m*x[0])*np.sin(np.pi*n*x[1])
    
    B = np.array([u0, u1, u2]) 
        
    return B


# Surface for the Poincare section
def Poincare_section(x, y):
    
    """
    Given two points x and y, check if the segment x --> y intersects the desired
    surface for the Poincare section and

      No intersection detected  --> return None
      One intersection detected --> return coordinates (u,v) of the intersection

    In this case the section is simply a vertical section at x[1] = 0.5 parametrized
    by coordinate u = x[0] and v = x[2]
    
    This method to detect the Poincare' plane avoid dependence on the time step.
    
    """

    delta_x = x[1] - 0.5
    delta_y = y[1] - 0.5

    if delta_x*delta_y < 0:
        # ... evaluate intersection ...
        t0 = (0.5 - x[1]) / (y[1] - x[1])
        u = x[0] + t0 * (y[0] - x[0])
        v = x[2] + t0 * (y[2] - x[2])
        return [u,v]
    elif delta_x != 0 and delta_y == 0:
        u = y[0]
        v = y[2]
        return [u,v]
    else:
        return None

