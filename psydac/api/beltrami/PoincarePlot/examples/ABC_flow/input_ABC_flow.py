"""
Configuration file for a Poincare' plot.

Test problem: The vector field is the ABC flow

Vx = A sin z + C cos y,
Vy = B sin x + A cos z,
Vz = C sin y + B cos x.


"""

from math import pi, sin, cos

# Output 
working_dir = '../examples/ABC_flow/'
output_file = 'test_ABC-flow'

# Parameter for the Poincare plot
dimensionality = 3
number_of_fieldlines = 1000
number_of_intersections = 200
lower_bounds = (0.0, 0.0, 0.0)
upper_bounds = (2.0*pi, 2.0*pi, 2.0*pi)


# Parameter for the ode integrator
period = 2.0 * pi ## (only for periodic domains)
max_time_steps = 10000
dt = 0.1

relative_tolerance = 1.e-10
absolute_tolerance = 1.e-10
max_substeps = 100

                      
# Definition of the vector field

# ... parameters of the vector field ...
A = 1.73205080757
B = 1.41421356237
C = 1.0

# ... callable ...
def vector_field(x):

    """
    ABC flow:

    Vx = A sin z + C cos y,
    Vy = B sin x + A cos z,
    Vz = C sin y + B cos x.
    """

    Vx = A * sin(x[2]) + C * cos(x[1])
    Vy = B * sin(x[0]) + A * cos(x[2])
    Vz = C * sin(x[1]) + B * cos(x[0])

    return [Vx, Vy, Vz]


# Surface for the Poincare section
def Poincare_section(x, y):
    
    """
    Given two points x and y, check if the segment x --> y intersects the desired
    surface for the Poincare section and

      No intersection detected  --> return None
      One intersection detected --> return coordinates (u,v) of the intersection

    In this case the section is simply a horizontal section at x[2] = pi parametrized
    by coordinate u = x[0] and v = x[1]
    """

    delta_x = x[2] - pi
    delta_y = y[2] - pi

    delta_0 = pi / 3.0
    
    if delta_x**2 < delta_0**2 and delta_y**2 < delta_0**2 and delta_x*delta_y < 0:
        # ... evaluate intersection ...
        t0 = (pi - x[2]) / (y[2] - x[2])
        u = x[0] + t0 * (y[0] - x[0])
        v = x[1] + t0 * (y[1] - x[1])
        return [u,v]
    elif delta_x != 0 and delta_y == 0:
        u = y[0]
        v = y[1]
        return [u,v]
    else:
        return None


