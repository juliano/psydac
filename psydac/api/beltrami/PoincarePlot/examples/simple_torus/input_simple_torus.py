"""
Configuration file for a Poincare' plot.

Test problem: Example of a vector field on the unit cube
with a family of nested toroidal flux surfaces.
"""

import numpy as np


# Output 
working_dir = '../examples/simple_torus/'
output_file = 'test_simple_torus'
output_file = 'test_simple_torus_higher_helicity'
save_fieldlines = True 
save_field_values = False


# Parameter for the Poincare plot
dimensionality = 3
number_of_fieldlines = 10
number_of_intersections = 1000
period = 1.0

# ... parameters of the vector field ...
delta = 0.5
Bt = 1.0
Bp = 0.7
R0 = 0.25
Z0 = 0.0
a = 0.2


# ... initial points for the fieldlines ...
jmax = number_of_fieldlines
initial_points = np.array([
    [delta + R0, delta, delta + Z0 + (j/jmax) * a]
    for j in range(jmax)
])


# Parameter for the ode integrator
max_time_steps = 10000000
dt = 0.001

relative_tolerance = 1.e-13
absolute_tolerance = 1.e-13
max_substeps = 1000

                      
# Definition of the vector field

# ... callable ...
def vector_field(x):

    """
    Example of flow with nested toroidal flux surfaces.
    """
    
    xx = x[0] - delta
    yy = x[1] - delta
    zz = x[2] - delta
    
    R = np.sqrt(xx**2 + yy**2)
    Z = zz

    s = np.sqrt( ((R - R0)**2 + (Z - Z0)**2) / a**2 ) 

    if s >= 1:

        B = [0.0, 0.0, 0.0]

    else:

        phi_prime_Jac = Bt * (1 - s)**2 / (R * a**2)
        chi_prime_Jac = Bp * s * (1 - s)**2 / (R * a**2)
    
        Bx = -phi_prime_Jac * yy - chi_prime_Jac * (Z-Z0) * xx / R
        By = +phi_prime_Jac * xx - chi_prime_Jac * (Z-Z0) * yy / R
        Bz =  chi_prime_Jac * (R-R0)
        
        B = [Bx, By, Bz]

    return B

# Surface for the Poincare section
def Poincare_section(x, y):
    
    """
    Given two points x and y, check if the segment x --> y intersects the desired
    surface for the Poincare section and

      No intersection detected  --> return None
      One intersection detected --> return coordinates (u,v) of the intersection

    In this case the section is simply a vertical section at x[1] = 0.5 parametrized
    by coordinate u = x[0] and v = x[2]
    """

    delta_x = x[1] - 0.5
    delta_y = y[1] - 0.5

    if delta_x*delta_y < 0:
        # ... evaluate intersection ...
        t0 = (0.5 - x[1]) / (y[1] - x[1])
        u = x[0] + t0 * (y[0] - x[0])
        v = x[2] + t0 * (y[2] - x[2])
        return [u,v]
    elif delta_x != 0 and delta_y == 0:
        u = y[0]
        v = y[2]
        return [u,v]
    else:
        return None
