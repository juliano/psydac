"""
Configuration file for a Poincare' plot.

Test problem: Example of a vector field on the unit cube
with axysymmetry.
"""

import numpy as np


# Output 
working_dir = '../examples/toroidal/'
output_file = 'test_toroidal-flow'

# Parameter for the Poincare plot
dimensionality = 3
number_of_fieldlines = 50
number_of_intersections = 500
period = 1.0

# ... parameters of the vector field ...
F0 = 10.0
r0 = 0.5
z0 = 0.5
a0 = 1.0
w0 = 1.0/8.0
k = 2.0

# ... initial points for the fieldlines ...
jmax = number_of_fieldlines
initial_points = np.array([
    [0.5*(1-0+r0), 0.5, 0.51 + (j/jmax-1) * (0.99 - 0.51)]
    for j in range(jmax)
])


# Parameter for the ode integrator
max_time_steps = 1000000
dt = 0.0001

relative_tolerance = 1.e-13
absolute_tolerance = 1.e-13
max_substeps = 1000

                      
# Definition of the vector field

# ... callable ...
def vector_field(x):

    """
    Example of flow with nested toroidal flux surfaces.
    """

    xx = 2.0*x[0] - 1.0
    yy = 2.0*x[1] - 1.0
    zz = x[2]
    
    r2 = xx**2 + yy**2
    r = np.sqrt(r2)
    
    Bx = -(2.0/r2) * (yy*F0 + xx * a0*k*np.pi*np.sin(k*np.pi*(zz-z0)))
    By =  (2.0/r2) * (xx*F0 - yy * a0*k*np.pi*np.sin(k*np.pi*(zz-z0)))
    Bz =  (4.0/r) * (r - r0)/w0**2 

    return [Bx, By, Bz]


# Surface for the Poincare section
def Poincare_section(x, y):
    
    """
    Given two points x and y, check if the segment x --> y intersects the desired
    surface for the Poincare section and

      No intersection detected  --> return None
      One intersection detected --> return coordinates (u,v) of the intersection

    In this case the section is simply a vertical section at x[1] = 0.5 parametrized
    by coordinate u = x[0] and v = x[2]
    """

    delta_x = x[1] - 0.5
    delta_y = y[1] - 0.5

    if delta_x*delta_y < 0:
        # ... evaluate intersection ...
        t0 = (0.5 - x[1]) / (y[1] - x[1])
        u = x[0] + t0 * (y[0] - x[0])
        v = x[2] + t0 * (y[2] - x[2])
        return [u,v]
    elif delta_x != 0 and delta_y == 0:
        u = y[0]
        v = y[2]
        return [u,v]
    else:
        return None
