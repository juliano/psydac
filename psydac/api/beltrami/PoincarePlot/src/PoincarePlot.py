"""
Generate the Poincare' plot of a given vector field in an n-dimensional cube
with a user-defined surface for the Poincare' section.

The dimension of the domain (and thus of the field) must be ndim > 2 
(strictly, that is, the vector field must at least be three dimensional).

The array of initial conditions for the fieldlines can be either specified in the
configuration module, or it is sampled from a uniform distribution over the 
domain. In the latter case, the domain must be specified as a box of the form 
[a1,b1]x[a2,b2]x...x[an,bn] for an n-dimensional problem. 

The use of the random initialization is not efficient since some of the 
fieldlines with random initial condition may not even intersect the chosen 
Poincare section.

Examples of configuration modules are provided in order to show how the vector 
field and the Poincare' section can be specified. 


USAGE:

  From terminal, in the /src directory:
  
    $ python PoincarePlot.py <relative path to configuration module> <nproc>

  where nproc is the number of threads; if not given, it is assumed nproc=1.

  This, if successful should produce an hdf5 output file in the directory
  specified in the configuration module. Examples of configuration modules
  are provided in /examples.

  Visualization of the Poincare plot

    $ python display_PoincarePlot.py <relative path to hdf5 output>

  The output hdf5 dataset contains the following data:

    1) Points along each fieldline where an intersection with the Poincare
       surface has been detected:

       'Poincare points', shape = (n_fieldlines, n_intersections, 2)

    2) All traced fieldline trajectories 
       (optional, controlled by save_fieldlines = True in the input module)
 
       'fieldlines', shape = (n_fieldlines, n_max_steps, n_dim)

    3) Values of the vector field on each fieldline:
       (optional, controlled by save_field_values = True in the input module)

       'vector field values', shape = (n_fieldlines, n_max_steps, n_dim)

  All datasets are initialized to np.nan.

  Here:

     n_fieldlines = number of fieldlines
     n_max_steps = max. number of time steps
     n_dim = dimensionality of the domain and vector field

  For the formatting and usage of the configuration modules, see the examples.
"""


import os
import sys
import h5py
import importlib
import datetime
import numpy as np
from scipy.integrate import ode
from p_tqdm import p_map


# Read inputs from a configuration file
if len(sys.argv) < 2:
    msg = 'An input file must be passed as second argument (i.e., argv[1]).'
    raise IOError(msg)

if len(sys.argv) == 3:
    nproc = int(sys.argv[2])
else:
    nproc = 1

configuration_file = sys.argv[1]
abs_path = os.path.abspath(configuration_file)
configuration_file_dir, cfile = os.path.split(abs_path)
sys.path.append(configuration_file_dir)
conf = importlib.import_module(cfile[0:-3])


# Define an array of initial conditions
if hasattr(conf, 'initial_points'):
    # ... use predefined set of points ...
    initial_pnts = conf.initial_points
else:
    # ... generate a uniform random distribution ...
    npts = conf.number_of_fieldlines
    ndim = conf.dimensionality
    l_bnds = conf.lower_bounds
    u_bnds = conf.upper_bounds
    initial_pnts = np.random.uniform(low=l_bnds, high=u_bnds, size=(npts,ndim))
    

# Load the vector field and the Poincare section
vectorV = conf.vector_field
check_intersection = conf.Poincare_section 

    
# Instance of the ODE integrator for the field lines
dt = conf.dt
n_fieldlines = initial_pnts.shape[0]
n_dim = initial_pnts.shape[1]
n_intersections = conf.number_of_intersections
n_max_steps = conf.max_time_steps

func = lambda t, x: vectorV(x)
tracer = ode(func)
tracer.set_integrator('dopri5',
                      rtol=conf.relative_tolerance,
                      atol=conf.absolute_tolerance,
                      nsteps=conf.max_substeps)


# Data files for the results
currtime = datetime.datetime.now().strftime("%Y-%m-%d_-_%H%M%S")
data_file_name = conf.working_dir+'/'+conf.output_file+'_-_'+currtime+'.hdf5'

print("\n --- Poincare' plot ---")
print(" ")
print("Output file name: {}".format(data_file_name))
print("Creating hdf5 dataset ...")

fid = h5py.File(data_file_name, 'w')

Poincare_plot = fid.create_dataset(
    'Poincare points', (n_fieldlines, n_intersections, 2), dtype='float64')


if hasattr(conf, 'save_fieldlines') and conf.save_fieldlines:
    fieldlines = fid.create_dataset(
        'fieldlines', (n_fieldlines, n_max_steps+1, n_dim), dtype='float64')
else:
    pass


if hasattr(conf, 'save_field_values') and conf.save_field_values:
    vector_values = fid.create_dataset(
        'vector field values', (n_fieldlines, n_max_steps+1, n_dim), dtype='float64')
else:
    pass

print("   ... hdf5 dataset initialized")



#### MAIN INTEGRATION LOOP ###
print("\nIntegration of field lines ...")


# Definition of the function to run in parallel
def engine(initial_pnt):

    intersection_counter = 0
    step_counter = 0

    # ... set initial condition ...
    tracer.set_initial_value(initial_pnt, 0.0)
    t = 0.0
    prev_position = tracer.y

    intersections = np.empty((n_intersections, 2))
    line = np.empty((n_max_steps+1, n_dim))
    values = np.empty((n_max_steps+1, n_dim))

    intersections[...] = np.nan
    line[...] = np.nan
    values[...] = np.nan
    
    if hasattr(conf, 'save_fieldlines') and conf.save_fieldlines:
        line[0,:] = prev_position # store initial condition
    else:
        pass

    if hasattr(conf, 'save_field_values') and conf.save_field_values:
        values[0,:] = vectorV(prev_position)
    else: 
        pass

    # main integration loop
    while intersection_counter < n_intersections and step_counter < n_max_steps:

        # ... integration of the field line ...
        tracer.integrate(t+dt)

        # ... updates ...
        t = tracer.t
        if hasattr(conf, 'period'):
            new_position = tracer.y % conf.period
        else:
            new_position = tracer.y

        # ... check for the intersection with the Poincare surface ...
        intersection_point = check_intersection(prev_position, new_position)
        if intersection_point != None:
            intersections[intersection_counter,:] = intersection_point
            intersection_counter += 1
        else:
            pass

        # ... store the current position ...
        prev_position = new_position
        step_counter += 1
            
        # ... save additional data on the fieldline ...
        if hasattr(conf, 'save_fieldlines') and conf.save_fieldlines:
            line[step_counter,:] = new_position
        else:
            pass
        
        if hasattr(conf, 'save_field_values') and conf.save_field_values:
            values[step_counter,:] = vectorV(new_position)
        else:
            pass

    return (intersections, line, values)

# Run in parallel
results = p_map(engine, initial_pnts, **{"num_cpus": nproc})

print("   ... all fieldlines traced.")


# WRITE OUTPUT

# loop over the field lines and store the results as appropriate
for iline in range(n_fieldlines):

    Poincare_plot[iline,:,:] = results[iline][0] # intersections
    
    if hasattr(conf, 'save_fieldlines') and conf.save_fieldlines:
        fieldlines[iline,:,:] = results[iline][1] # fieldlines
    else:
        pass
    if hasattr(conf, 'save_field_values') and conf.save_field_values:
        vector_values[iline,:,:] = results[iline][2] # values
    else:
        pass

    
print("Output written in: {}".format(data_file_name))
print("--- Mission accomplished ---")

# Finalizations 
fid.close()

