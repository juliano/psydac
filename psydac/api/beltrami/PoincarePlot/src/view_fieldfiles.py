"""
Visualization of selected fieldlines.

USAGE: 

   $ python view_fieldlines <path to hdf5 dataset> iline it_max

 where iline and it_max are optional integers representing the index
 of the field line to plot (default iline=0) and the max time index to
 plot (default plots the whole line).
"""


import sys
import h5py
import numpy 

from mayavi import mlab


# Read inputs from a configuration file
if len(sys.argv) < 2:
    print(__doc__)
    msg = 'An hdf5 file must be passed as second argument (i.e., argv[1]).'
    raise IOError(msg)

data_file = sys.argv[1]
fid = h5py.File(data_file, 'r')
fieldlines = fid.get('fieldlines')[()]
fid.close()

if len(sys.argv) > 2:
    if sys.argv[2] == 'all':
        nlines = fieldlines.shape[0]
        selected_lines = numpy.arange(nlines, dtype=int)
    else:
        iline = int(sys.argv[2])
        selected_lines = [iline,]
else:
    selected_lines = [0,]

colors = [(0.7,0,0), (0,0.7,0), (0,0,0.7), (0.7,0,0.7), (0,0.7,0.7), (0.7,0.7,0)]

mlab.options.offscreen = True

fig = mlab.figure(1, size=(1200,1000))

# Plot the y=0.5 surface
s = numpy.linspace(0.0,1.0,20)
x, z = numpy.meshgrid(s, s)
y = 0.5 * numpy.ones_like(x)
mlab.mesh(x,y,z, opacity=0.9, color=(0.8,0.8,0.8))

for iline in selected_lines:

    if len(sys.argv) > 3:
        it_max = int(sys.argv[3])
    else:
        # Use the first coordinate to find the extent of the data
        # (The array is filled with numpy.nan if the integration stops early.)
        xx = fieldlines[iline,~numpy.isnan(fieldlines[iline,:,0]),0]
        it_max = xx.size

    mlab.plot3d(fieldlines[iline,0:it_max,0],
                fieldlines[iline,0:it_max,1],
                fieldlines[iline,0:it_max,2],
                color=colors[iline % 6],
                tube_radius=0.001,
                figure=fig)

#mlab.show()
mlab.savefig('rename.png')
