"""
Visualization of the Poincare' plot.
"""


import sys
import h5py
import matplotlib.pyplot as plt


# Read inputs from a configuration file
if len(sys.argv) < 2:
    msg = 'An hdf5 file must be passed as second argument (i.e., argv[1]).'
    raise IOError(msg)

data_file = sys.argv[1]
fid = h5py.File(data_file, 'r')
points = fid.get('Poincare points')[()]
fid.close()

nlines = points.shape[0]

fig = plt.figure(1)
ax = fig.add_subplot(111)

for iline in range(0,nlines):
    ax.scatter(points[iline,:,0], points[iline,:,1], marker='.', s=5)

plt.show()
