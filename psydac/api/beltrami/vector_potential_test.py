import  time
import  h5py
import  numpy                as np
import  matplotlib.pyplot    as plt
from    mpi4py               import MPI

from sympde.topology import Cube
from sympde.topology import Derham
from sympde.topology import elements_of
from sympde.expr     import BilinearForm, integral
from sympde.calculus import dot

from psydac.linalg.block            import BlockVectorSpace, BlockVector, BlockLinearOperator
from psydac.linalg.basic            import ZeroOperator
from psydac.linalg.solvers          import inverse
from psydac.api.discretization      import discretize
from psydac.api.settings            import PSYDAC_BACKEND_GPYCCEL
from psydac.api.beltrami.output     import get_np_data, get_coefficients

### Idea: Load b field from file, then compute the vector potential using different solvers, both serial and parallel

filename = 'fields_3e-4_Nm5.h5'
ncells = [32, 32, 32]
degree = [2, 2, 2]
periodic = [True, True, True]

def main(filename, ncells, degree, periodic):

    comm = MPI.COMM_WORLD
    backend = PSYDAC_BACKEND_GPYCCEL

    derham_h, G, C, D, M1, M2, M3 = get_utils(ncells, degree, periodic, comm, backend)
    Ct = C.T
    Gt = G.T
    V0 = derham_h.V0.vector_space
    V1 = derham_h.V1.vector_space
    V10 = BlockVectorSpace(V1, V0)
    VPM_inverse_cg = get_VPM_inverse(C, Ct, G, Gt, M1, M2, V10, V0, solver='cg')
    #VPM_inverse_bicg = get_VPM_inverse(C, Ct, G, Gt, M1, M2, V10, V0, solver='bicg')
    #VPM_inverse_bicgstab = get_VPM_inverse(C, Ct, G, Gt, M1, M2, V10, V0, solver='bicgstab')
    VPM_inverse_minres = get_VPM_inverse(C, Ct, G, Gt, M1, M2, V10, V0, solver='minres')
    #VPM_inverse_lsmr = get_VPM_inverse(C, Ct, G, Gt, M1, M2, V10, V0, solver='lsmr')
    #VPM_inverse_gmres = get_VPM_inverse(C, Ct, G, Gt, M1, M2, V10, V0, solver='gmres')
    if comm.Get_rank() == 0:
        print(f"Inverse created!")
    coeff_list = get_b_field(filename, derham_h)

    Ts, Ds, Es = get_plot_data(coeff_list, D, M2, M3, filename)

    ### CG ###
    comm.Barrier()
    if comm.Get_rank() == 0:
        print(f"Computing A using a CG solver ...")
        start = time.time()
    for n in range(len(coeff_list)):
        b = coeff_list[n]
        a_cg = vector_potential(b, M2, Ct, V0, V10, VPM_inverse_cg)
        print(VPM_inverse_cg.get_info())
    comm.Barrier()
    if comm.Get_rank() == 0:
        stop = time.time()
        total = stop - start
        print(f"Done!")
        print(f"--- CG ---: time {total} seconds")
        print()

    ### BICG ###
    #comm.Barrier()
    #if comm.Get_rank() == 0:
    #    print(f"Computing A using a BICG solver ...")
    #    start = time.time()
    #a_bicg = vector_potential(coeff_list[-1], M2, Ct, V0, V10, VPM_inverse_bicg, comm)
    #comm.Barrier()
    #if comm.Get_rank() == 0:
    #    stop = time.time()
    #    total = stop - start
    #    print(f"Done!")
    #    print(f"--- BICG ---: norm {np.linalg.norm(a_bicg.toarray())}, time {total} seconds")
    #    print(VPM_inverse_bicg.get_info())
    #    print()

    ### BICGSTAB ###
    #comm.Barrier()
    #if comm.Get_rank() == 0:
    #    print(f"Computing A using a BICGSTAB solver ...")
    #    start = time.time()
    #a_bicgstab = vector_potential(coeff_list[-1], M2, Ct, V0, V10, VPM_inverse_bicgstab, comm)
    #comm.Barrier()
    #if comm.Get_rank() == 0:
    #    stop = time.time()
    #    total = stop - start
    #    print(f"Done!")
    #    print(f"--- BICGSTAB ---: norm {np.linalg.norm(a_bicgstab.toarray())}, time {total} seconds")
    #    print(VPM_inverse_bicgstab.get_info())
    #    print()

    ### MINRES ###
    comm.Barrier()
    if comm.Get_rank() == 0:
        print(f"Computing A using a MINRES solver ...")
        start = time.time()
    for n in range(len(coeff_list)):
        b = coeff_list[n]
        a_minres = vector_potential(b, M2, Ct, V0, V10, VPM_inverse_minres)
        print(VPM_inverse_minres.get_info())
    comm.Barrier()
    if comm.Get_rank() == 0:
        stop = time.time()
        total = stop - start
        print(f"Done!")
        print(f"--- MINRES ---: time {total} seconds")
        print()

    ### LSMR ###
    #comm.Barrier()
    #if comm.Get_rank() == 0:
    #    print(f"Computing A using a LSMR solver ...")
    #    start = time.time()
    #a_lsmr = vector_potential(coeff_list[-1], M2, Ct, V0, V10, VPM_inverse_lsmr)
    #comm.Barrier()
    #if comm.Get_rank() == 0:
    #    stop = time.time()
    #    total = stop - start
    #    print(f"Done!")
    #    print(f"--- LSMR ---: norm {np.linalg.norm(a_lsmr.toarray())}, time {total} seconds")
    #    print(VPM_inverse_lsmr.get_info())
    #    print()

    ### GMRES ###
    #comm.Barrier()
    #if comm.Get_rank() == 0:
    #    print(f"Computing A using a GMRES solver ...")
    #    start = time.time()
    #a_gmres = vector_potential(coeff_list[-1], M2, Ct, V0, V10, VPM_inverse_gmres)
    #comm.Barrier()
    #if comm.Get_rank() == 0:
    #    stop = time.time()
    #    total = stop - start
    #    print(f"Done!")
    #    print(f"--- GMRES ---: norm {np.linalg.norm(a_gmres.toarray())}, time {total} seconds")
    #    print(VPM_inverse_gmres.get_info())
    #    print()

    make_plot(Ts, Ds, Es, 'test')
    
def get_b_field(filename, derham_h):

    with h5py.File(filename, "r") as f:

        snaps = len(list(f.keys()))-1

    coeff_list = []

    for n in range(snaps):
        data = get_np_data(filename, n+1)
        coeffs = get_coefficients(derham_h.V2.vector_space, data)
        coeff_list.append(coeffs)

    return coeff_list

def get_utils(ncells, degree, periodic, comm, backend):

    domain = Cube('C', bounds1=(0, 1), bounds2=(0, 1), bounds3=(0, 1))
    derham = Derham(domain)

    domain_h = discretize(domain, ncells=ncells, periodic=periodic, comm=comm)
    derham_h = discretize(derham, domain_h, degree=degree)

    G, C, D = derham_h.derivatives_as_matrices

    u1, v1 = elements_of(derham.V1, names='u1, v1')
    u2, v2 = elements_of(derham.V2, names='u2, v2')
    u3, v3 = elements_of(derham.V3, names='u3, v3')

    a1 = BilinearForm((u1, v1), integral(domain, dot(u1, v1)))
    a2 = BilinearForm((u2, v2), integral(domain, dot(u2, v2)))
    a3 = BilinearForm((u3, v3), integral(domain, u3 * v3))

    start = time.time()
    a1_h = discretize(a1, domain_h, (derham_h.V1, derham_h.V1), backend=backend)
    stop = time.time()
    if comm.Get_rank() == 0:
        print(f"discretize a1: {stop-start} seconds") 
    M1 = a1_h.assemble()

    start = time.time()
    a2_h = discretize(a2, domain_h, (derham_h.V2, derham_h.V2), backend=backend)
    stop = time.time()
    if comm.Get_rank() == 0:
        print(f"discretize a2: {stop-start} seconds") 
    M2 = a2_h.assemble()

    start = time.time()
    a3_h = discretize(a3, domain_h, (derham_h.V3, derham_h.V3), backend=backend)
    stop = time.time()
    if comm.Get_rank() == 0:
        print(f"discretize a3: {stop-start} seconds") 
    M3 = a3_h.assemble()

    return derham_h, G, C, D, M1, M2, M3

def get_plot_data(coeff_list, D, M2, M3, filename):

    l = len(coeff_list)

    Ts = []
    Ds = []
    Es = []

    for n in range(l):
        coeffs = coeff_list[n]
        divergence = _divergence(coeffs, D, M3)
        entropy = _entropy(coeffs, M2)
        with h5py.File(filename, "r") as f:
            snapshot = list(f.keys())[n+1]
            t = f[snapshot].attrs['t']

        Ts.append(t)
        Ds.append(divergence)
        Es.append(entropy)

    return Ts, Ds, Es

def _divergence(b, D, M3):

    div = D @ b
    out = np.sqrt( ( M3 @ div ).dot(  div) )

    return out

def _entropy(b, M2):

    out = ( M2 @ b ).dot( b )
    out *= 0.5

    return out

def make_plot(Ts, Ds, Es, label):

    fin_t = Ts[-1]
    #h0 = 
    #rel_h_err = [abs(h0-h[i])/h0 for i in range(len(h))]

    fig, axs = plt.subplots(2, 2, figsize=(24, 8))

    axs[1, 0].plot(Ts, Ds, '^', label=label)
    axs[0, 1].plot(Ts, Es, '^', label=label)

    #axs[0, 0].plot(t, rel_h_err, color='c', label=label)
    #axs[0, 1].plot(t, s, color='c', label=label)
    #axs[1, 0].plot(t, d, color='c', label=label)
    #axs[1, 1].plot(tj, j, color='c', label=label)

    #for n in range(l):
    #    axs[1, 0].plot(Ts[n], Ds[n], '^')
    #    axs[0, 1].plot(Ts[n], Es[n], '^')

    #axs[0, 0].legend()
    axs[0, 1].legend()
    axs[1, 0].legend()
    #axs[1, 1].legend()

    #axs[0, 0].set_xlim([0, fin_t])
    #axs[0, 0].set_xlabel('t')
    axs[0, 1].set_xlim([0, fin_t])
    axs[0, 1].set_xlabel('t')
    axs[1, 0].set_xlim([0, fin_t])
    axs[1, 0].set_xlabel('t')
    #axs[1, 1].set_xlim([0, fin_t])
    #axs[1, 1].set_xlabel('t')

    #axs[0, 0].set_ylim([1e-15, 1e-10])
    #axs[0, 0].set_yscale('log')
    axs[0, 1].set_ylim([6.25, 6.6])
    axs[1, 0].set_ylim([1e-13, 1e-8])
    axs[1, 0].set_yscale('log')
    #axs[1, 1].set_ylim([0, 2500])

    #axs[0, 0].set_title('| H(B_t) - H(B_0) | / H(B_0)')
    axs[0, 1].set_title('S(B_t)')
    axs[1, 0].set_title('|| div(B_t) ||')
    #axs[1, 1].set_title('|| J x H ||')

    plt.show()

def vector_potential(b, M2, Ct, V0, V10, VPM_inverse):
        """Computes the coefficient vector a of A in V_h^1 s.th. curl A = B, weak-div A = 0. b being the coefficients of B."""

        rhs0 = Ct @ (M2 @ b)
        rhs1 = V0.zeros()
        rhs = BlockVector(V10, (rhs0, rhs1))

        out = VPM_inverse @ rhs

        a = out[0]

        return a

def get_VPM_inverse(C, Ct, G, Gt, M1, M2, V10, V0, solver):
        
    VPM00 = Ct @ (M2 @ C)
    VPM01 = M1 @ G
    VPM10 = Gt @ M1
    VPM11 = ZeroOperator(V0, V0)
    VPM = BlockLinearOperator(V10, V10, ((VPM00, VPM01), (VPM10, VPM11)))

    x0_vpm = V10.zeros()
    VPM_inverse = inverse(VPM, solver=solver, x0=x0_vpm, tol=1e-12, maxiter=2000, recycle=True)

    return VPM_inverse

main(filename, ncells, degree, periodic)