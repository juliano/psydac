from psydac.api.beltrami.relax import relax, restart_relax

ncells = [32, 32, 32]
degree = [2, 2, 2]
N_min = 7

maxtime = 1e-6

save = False
#amount = 20

restart = False
#restart_field = 'data/fields/fields_3e-3.h5'
#save_field_in = 'data/fields/fields_5e-3.h5'
#restart_diagnostics = 'diagnostics/nc32_3e-3.txt'

hamiltonian_at = 20

if save:
    if restart:
        file = restart_diagnostics
        f = open(file, 'r')
        lines = f.readlines()
        last_line = lines[-1].split()
        t_start = float(last_line[1])
        save_b_field_at = [t_start + (i+1)*(maxtime-t_start)/amount for i in range(amount)]
        save_b_field_at[-1] = maxtime
    else:
        save_b_field_at = [(i+1)*maxtime/amount for i in range(amount)]
else:
    save_b_field_at = None

if restart:
    restart_relax(restart_field, restart_diagnostics, maxtime, ncells, degree, save_b_field_at, save_field_in, N_min, hamiltonian_at)
else:
    relax(maxtime, ncells, degree, save_b_field_at, N_min, hamiltonian_at)
