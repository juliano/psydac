How to use **/beltrami**
========================

The purpose of this submodule is to relax a given initial field with nonzero magnetic helicity to an equilibrium state of simplified MHD equations (for more see thesis).

The core element of the module is `beltrami.py`, and in particular its only content, the `class Beltrami`. Its main feature is the method `integrate`, which takes field coefficients as input and integrates them in time, according to the relaxation model mentioned above. The simulation is meant to be executed using the convenience script `test_beltrami.py`.

As of 28.08.2023, field coefficients can be saved in an `.h5` file and the relaxation process can be interrupted and restarted at a later point in time. Space information is stored in a file called `spaces.yml`, but I don't currently make use of it.

In addition to coefficient and space information, a `.txt` file is created containing information about the computation time as well as diagnostic values, in particular the relative hamiltonian error, entropy, L2-norm of the divergence and equilibrium condition.

Using saved coefficients, Poincare and fieldline plots can be created using the subsubmodule `/Poincare` and in particular its scripts `src/PoincarePlot.py` followed by `src/display_PoincarePlot.py` and `src/view_fieldfiles.py`.

The diagnostic values can be visualized using the two methods defined in `beltrami_plot.py` and in particular via the convenience script `plot.py`.

**Workflow**
------------

1. Initial condition

    Define an initial vector potential A (`get_A_fun()`) in `initial_condition.py`. The corresponding field `B = curl A` should have nonzero magnetic helicity, as else the field might relax to the trivial zero solution.

2. Discretization parameters

    In `test_beltrami.py`, choose the discretization parameters `ncells & degree` as well as optionally `N_min`. The first two must be a tuple or list of length 3 containing the amount of Bspline cells and the Bspline degree in each spatial direction. `N_min` is relevant for the computation of the step size. Starting with `dt0 = 1.5e-8`, the stepsize increases (or decreases) by a factor of `f_update = 1.5` every time `<= N_min` (or `>= N_max`) Picard iterations were necessary to find the fixed point. As done by Camilla, I use `N_min = 5`, but numerical experiments should be conducted to verify whether a higher value results in similar results, as the computation time decreases rapidly the higher `N_min`.

3. Remaining parameters in `test_beltrami.py`

     - maxtime
     - save
     - amount
     - restart
     - restart_field
     - save_field_in
     - restart_diagnostics
     - hamiltonian_at

    `maxtime` is the maximum time until which the initial field is integrated in time. A maximum equilibrium condition value might be a better break condition.

    `save` is a boolean variable: `True` means that field coefficients are being saved in an `.h5` file, `False` means the opposite.

    `amount` is the amount of coefficients saved, uniformily distributed in the simulation time interval. For example, I decided to save coefficients every 5e-5. Given a maxtime of 3e-4, that means I set `amount = 6`.

    `restart` is a boolean variable: `True` means that we restart the relaxation process given previously saved field coefficients as well as diagnostic values, `False` means the opposite.

    `restart_field` is a path to the `.h5` file from which the last saved coefficients are being used to restart the simulation. For example `restart_field = 'data/fields/Raven/5e-1_to_7e-1.h5'`. 

    Given this setup, if I wanted to further integrate until 1.0 (from 7e-1), and given that I want to save coefficients every 5e-3, I'd set maxtime = 1.0, save = True, amount = (1.0 - 7e-1) / 5e-3 = 60, restart = True, restart_field = 'data/fields/Raven/5e-1_to_7e-1.h5'.

    `save_field_in` is a path to a yet nonexisting `.h5` file where the to be computed coefficients are being saved in. For example, `save_field_in = 'data/fields/Raven/7e-1_to_1.h5'`.

    `restart_diagnostics` is the path to a diagnostics `.txt` file corresponding to the `.h5` file specified in `restart_field`. From the last line of this file, the stepsize, the runtime and and current computation time are being read. For example `restart_diagnostics = 'diagnostics/Raven/7e-1.txt'`.

    `hamiltonian_at` is an integer value specifying at which time steps the costly hamiltonian is being computed. For example, `hamiltonian_at = 20` means that the hamiltonian is only computed at every 20th time step.

4. Start the simulation

    In `psydac/api/beltrami`, execute `mpirun -n [n] python test_beltrami.py`, where `[n]` is to be replaced by an integer specifying the amount of processes assigned to the computation.

**Visualizing diagnostics**
---------------------------

The file `beltrami_plot.py` handles the visualization of diagnostic values. However, the convenience script `plot.py` is to be used to manage the parameter input. You can monitor your simulation while it is still running, as the diagnostics `.txt` file gets updated after every time step.

1. In `plot.py`, specify plot parameters

    - compare
    - snapshots
    - onlytwo
    - txt1, txt2, txt3
    - label1, label2, label3

    `compare` is a boolean variable. Internally, decides whether `beltrami_plot.plot` or `beltrami_plot.plot_compare` is called. If `True`, returns a plot comparing two or three diagnostic files, specified by `txt1, txt2, txt3` and `label1, label2, label3`. 

    `snapshots` is either `None` or a list of paths, directing to `.h5` files containing saved field coefficients. Only relevant if `compare = False`. If specified, adds the diagnostics values of the given snapshots (=coefficients) to the plot. Meant as a way of making sure that the saved coeffiecients are in fact the right ones.

    `onlytwo` is a boolean variable. Only relevant if `compare = True`. If `True`, returns, returns a plot comparing only two diagnostic `.txt` files. Will be removed in the future. 

2. Plot

    In `psydac/api/beltrami`, execute `python plot.py`.

3. Two examples

        compare = True\
        #snapshots = None\
        onlytwo = False

        txt1 = 'diagnostics/nc32_3e-4_old.txt'\
        label1 = 'nc32'\
        txt2 = 'diagnostics/nc16_3e-4_old.txt'\
        label2 = 'nc16'\
        txt3 = 'diagnostics/nc8_3e-4_old.txt'\
        label3 = 'nc8'

    results in the following plot. With the addition of the variable `hamiltonian_at` recently, the first plot won't look as messy in the future.

    ![](psydac/api/beltrami/diagnostics/example1_3e-4_nc8-16-32.png)

        compare = False
        snapshots = ['data/fields/Raven/0_to_3e-4.h5', 
                    'data/fields/Raven/3e-4_to_1e-2.h5', 
                    'data/fields/Raven/1e-2_to_1e-1.h5',
                    'data/fields/Raven/1e-1_to_2e-1.h5',
                    'data/fields/Raven/2e-1_to_3e-1.h5',
                    'data/fields/Raven/3e-1_to_5e-1.h5',
                    'data/fields/Raven/5e-1_to_7e-1.h5',
                    'data/fields/Raven/7e-1_to_1.h5]
        #onlytwo = False

        txt1 = 'diagnostics/Raven/1e0.txt'
        label1 = 'nc32 - p=2 - N_min=5'
        #txt2 = 'diagnostics/nc16_3e-4_old.txt'
        #label2 = 'nc16'
        #txt3 = 'diagnostics/nc8_3e-4_old.txt'
        #label3 = 'nc8'

    results in the following plot. The triangles in the top right and bottom left plot are the added snapshots. Them aligning with the plot indicates that the chosen snapshots belong to the chosen diagnostics `.txt` file.

    ![](psydac/api/beltrami/diagnostics/example2_1e0.png)

**Poincarè & Fieldline Plot**
-----------------------------

! Currently, I only adapted the file `PoincarePlot/examples/tunable_helicity_field/input_tunable_helicity_field_central_island.py` to be compatible with my code. The adapted version is `PoincarePlot/examples/tunable_helicity_field/relaxed_central_island.py` !

To obtain both a Poincarè & a fieldline plot of a specific field, this field has to be adressed inside the `relaxed_central_island.py` file. This must be done setting two variables: First, specify the correct `.h5` file containing the right coefficients in line 75: 

    restart_field = '../../data/fields/Raven/3e-4_to_1e-2.h5'   

Second, choose the right coefficients from inside the `.h5` file. This happens between lines 53 and 57. Choose either 

    with h5py.File(filename, "r") as f:

       snaps = len(list(f.keys()))-1

to adress the last saved coefficients, or choose 

    snaps = [n]

where `[n]` is to be replaced by an integer, to choose the n-th stored coefficients.

You use the following lines to determine the "time" corresponding to a set of saved coefficients:

    with h5py.File(restart_field, "r") as f:
        snapshot = list(f.keys())[ [n] ]
        print( f[snapshot].attrs['t'] )

where again `[n]` is to be replaced by an integer and restart_field by an `.h5` file containing coefficients.

1. Obtaining the necessary `.hdf5` file

    In `psydac/api/beltrami/PoincarePlot/src`, execute `python PoincarePlot.py ../examples/tunable_helicity_field/relaxed_central_island.py [n]`

    where `[n]` is again to be replaced by an integer representing the amount of processes assigned to the computation. Using `n=4`, this takes about 45 minutes on my machine. The file generated is called `test_tunable_helicity_field_-_[date]_-_[time].hdf5`. For now, I manually give it a different name and move it, for example, to `data/Poincare/hdf5_files/`.

2. Obtaining the Poincarè plot

    In `psydac/api/beltrami/PoincarePlot/src`, execute `python display_PoincarePlot.py ../../data/Poincare/hdf5_files/[filename].hdf5`,

    where `[filename]` is to be replaced with the actual filename. I save the obtained Poincarè plot in `data/Poincare/PoincarePlots`.

3. Obtaining the fieldline plot

    In `psydac/api/beltrami/PoincarePlot/src`, execute `python view_fieldfiles.py ../../data/Poincare/hdf5_files/[filename].hdf5`,

    where `[filename]` is to be replaced with the actual filename. A `.png` file called `rename` is created in `src/`. I rename and save the obtained fieldline plot in `data/Poincare/FieldlinePlots`.

Below, find some example Poincarè & fieldline plots:

t = 0.
![](psydac/api/beltrami/data/Poincare/FieldlinePlots/0,00.png)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/0,00.png)
t = 0.0005
![](psydac/api/beltrami/data/Poincare/FieldlinePlots/0,50e-3.png)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/0,50e-3.png)
t = 0.0010
![](psydac/api/beltrami/data/Poincare/FieldlinePlots/1,0e-3.png)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/1,0e-3.png)
t = 0.0015
![](psydac/api/beltrami/data/Poincare/FieldlinePlots/1,50e-3.png)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/1,50e-3.png)
t = 0.0020
![](psydac/api/beltrami/data/Poincare/FieldlinePlots/2,0e-3.png)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/2,0e-3.png)
t = 0.0025
![](psydac/api/beltrami/data/Poincare/FieldlinePlots/2,50e-3.png)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/2,50e-3.png)
t = 0.0030
![](psydac/api/beltrami/data/Poincare/FieldlinePlots/3,0e-3.png)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/3,0e-3.png)

![](psydac/api/beltrami/data/Poincare/FieldlinePlots/animation_3e-3.gif)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/animation_3e-3.gif)

The latest plots that I was able to generate correspond to the relaxation time `t = 0.035`. The fieldline looks more complex than the previous ones.

t = 0.035
![](psydac/api/beltrami/data/Poincare/FieldlinePlots/additional_plots/3,5e-2.png)
![](psydac/api/beltrami/data/Poincare/PoincarePlots/additional_plots/3,5e-2.png)
