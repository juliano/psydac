from mpi4py import MPI
import numpy as np
import time

from sympde.calculus import dot, cross
from sympde.topology import elements_of
from sympde.topology import Derham
from sympde.expr     import BilinearForm, integral

from psydac.fem.basic               import FemField
from psydac.api.discretization      import discretize
from psydac.api.settings            import PSYDAC_BACKEND_GPYCCEL
from psydac.api.beltrami.m1solver   import get_M1_block_kron_solver
from psydac.api.beltrami.output     import get_Om, add_snapshot
from psydac.linalg.solvers          import inverse
from psydac.linalg.block            import BlockVectorSpace, BlockVector, BlockLinearOperator

class Beltrami():

    def __init__(self, logical_domain, ncells, degree, periodic, 
                 maxtime, save_b_field_at, hamiltonian_at, 
                 N_min, N_max=40, maxiter=100, f_update=1.5, 
                 dt_max=1e-03, dt0=1.5e-08, err_tol=1e-13, 
                 picard_solver='cg', picard_pc=None, picard_tol=1e-12, picard_maxiter=1000, 
                 mapping=None, 
                 comm=MPI.COMM_WORLD, backend=PSYDAC_BACKEND_GPYCCEL):

        comm = MPI.COMM_WORLD
        backend = PSYDAC_BACKEND_GPYCCEL

        # Parallelization
        mpi_size = comm.Get_size()
        mpi_rank = comm.Get_rank()
        self._comm = comm
        self._mpi_size = mpi_size
        self._mpi_rank = mpi_rank
        self._backend = backend

        # Continuous domain and derham sequence
        self._logical_domain = logical_domain
        if mapping is None:
            domain = logical_domain
            derham = Derham(domain)
            self._domain = domain
            self._derham = derham
        else:
            raise ValueError(f"mapping must be None for now.")
        
        # maxtime = max evolution time, maxiter = max picard iterations before assert error is raised
        self._maxtime = maxtime
        self._maxiter = maxiter

        # these quantities govern the stepsize regulation
        self._N_min = N_min
        self._N_max = N_max
        self._f_update = f_update

        # maximum and initial stepsize
        self._dt_max = dt_max
        self._dt0 = dt0

        # tolerance not for the picard solver, but for the picard step, i.e., | \hat{B}_h^{p+1} - \hat{B}_h^p | <= err_tol is required
        self._err_tol = err_tol

        # the picard solver solves the complex linear system during each picard step
        self._picard_solver = picard_solver
        self._picard_pc = picard_pc
        self._picard_tol = picard_tol
        self._picard_maxiter = picard_maxiter
        
        # discrete domain and derham sequence
        start = time.time()
        domain_h = discretize(domain, ncells=ncells, periodic=periodic, comm=comm)
        stop = time.time()
        if mpi_rank == 0:
            print(f"discretize domain: {stop-start} seconds")

        start = time.time()
        derham_h = discretize(derham, domain_h, degree=degree)
        stop = time.time()
        if mpi_rank == 0:
            print(f"discretize derham: {stop-start} seconds")

        self._domain_h = domain_h
        self._derham_h = derham_h

        # discretization of bilinear forms
        V0 = derham_h.V0.vector_space
        V1 = derham_h.V1.vector_space
        V2 = derham_h.V2.vector_space
        V3 = derham_h.V3.vector_space
        V10 = BlockVectorSpace(V1, V0)

        self._V0 = V0
        self._V1 = V1
        self._V2 = V2
        self._V3 = V3
        self._V10 = V10

        u1, v1 = elements_of(derham.V1, names='u1, v1')
        u2, v2 = elements_of(derham.V2, names='u2, v2')
        u3, v3 = elements_of(derham.V3, names='u3, v3')

        a1 = BilinearForm((u1, v1), integral(domain, dot(u1, v1)))
        a2 = BilinearForm((u2, v2), integral(domain, dot(u2, v2)))
        a3 = BilinearForm((u3, v3), integral(domain, u3 * v3))
        a12 = BilinearForm((u1, u2), integral(domain, dot(u1, u2)))

        start = time.time()
        a1_h = discretize(a1, domain_h, (derham_h.V1, derham_h.V1), backend=backend)
        stop = time.time()
        if mpi_rank == 0:
            print(f"discretize a1: {stop-start} seconds")
        self._M1 = a1_h.assemble()

        start = time.time()
        a2_h = discretize(a2, domain_h, (derham_h.V2, derham_h.V2), backend=backend)
        stop = time.time()
        if mpi_rank == 0:
            print(f"discretize a2: {stop-start} seconds")
        self._M2 = a2_h.assemble()

        start = time.time()
        a3_h = discretize(a3, domain_h, (derham_h.V3, derham_h.V3), backend=backend)
        stop = time.time()
        if mpi_rank == 0:
            print(f"discretize a3: {stop-start} seconds")
        self._M3 = a3_h.assemble()

        start = time.time()
        a12_h = discretize(a12, domain_h, (derham_h.V2, derham_h.V1), backend=backend)
        stop = time.time()
        if mpi_rank == 0:
            print(f"discretize a12: {stop-start} seconds")
        self._M12 = a12_h.assemble()

        # Diff operators and projectors
        G, C, D = derham_h.derivatives_as_matrices
        Ct = C.T
        Gt = G.T
        Dt = D.T

        P0, P1, P2, P3  = derham_h.projectors()

        self._G = G
        self._Gt = Gt
        self._C = C
        self._Ct = Ct
        self._D = D
        self._Dt = Dt
        self._P0 = P0
        self._P1 = P1
        self._P2 = P2
        self._P3 = P3

        # x0 for the complex picard linear system
        self._x0_s = V2.zeros()

        ### use different M1_solvers for different tasks?
        M1_solver = get_M1_block_kron_solver(V1, ncells, degree, periodic)
        self._M1_solver = M1_solver

        Jmat = M1_solver @ Ct @ self._M2
        self._Jmat = Jmat

        # system matrix for the computation of the vector potential A \in V_h^1 of a div.-free element B \in V_h^2
        VPM00 = self._Ct @ (self._M2 @ self._C)
        VPM01 = self._M1 @ self._G
        VPM10 = self._Gt @ self._M1
        VPM11 = None
        VPM = BlockLinearOperator(V10, V10, ((VPM00, VPM01), (VPM10, VPM11)))

        x0_vpm = V10.zeros()
        VPM_inverse = inverse(VPM, solver='minres', x0=x0_vpm, tol=1e-12, maxiter=2000, recycle=True)

        self._VPM_inverse = VPM_inverse

        # DiscreteBilinearForm q_h for the computation of Q

        u1, u2, h1, J, H = elements_of(derham.V1, names='u1, u2, h1, J, H')

        if mpi_rank == 0:
            print(f"discretizing q ...")
        start = time.time()
        q = BilinearForm((u1, u2), integral(domain, dot( cross(h1, u1), cross(h1, u2) )))
        q_h = discretize(q, domain_h, (derham_h.V1, derham_h.V1), backend=backend)
        self._q_h = q_h
        stop = time.time()
        if mpi_rank == 0:
            print(f"... {stop-start} seconds")

        # for the computation of the equilibrium condition J x H
        if mpi_rank == 0:
            print(f"discretizing jxh ...")
        start = time.time()
        jxh = BilinearForm((u1, u2), integral(domain, dot( cross(J, u1), cross(u2, H) )))
        jxh_h = discretize(jxh, domain_h, (derham_h.V1, derham_h.V1), backend=backend)
        self._jxh_h = jxh_h
        stop = time.time()
        if mpi_rank == 0:
            print(f"... {stop-start} seconds")

        self._save_b_field_at = save_b_field_at

        self._hamiltonian_at = hamiltonian_at

    def integrate(self, b, restart=False, save_field_in=None, restart_diagnostics=None):
        """ Evolves b. See thesis. Add more explanation later. """

        if restart:
            n_re, t_re, dt_re, runtime_re = self._get_restart_data(restart_diagnostics)

        maxtime = self._maxtime
        maxiter = self._maxiter
        if restart:
            dt = dt_re
        else:
            dt = self._dt0
        err_tol = self._err_tol

        save_b_field_at = self._save_b_field_at
        if save_b_field_at is not None:
            Om = get_Om(self._derham_h.V2, self._comm, save_field_in=save_field_in)
            save_count = 0
            save_time = save_b_field_at[save_count]
            save_len = len(save_b_field_at)

        if self._mpi_rank == 0:
            if not restart:
                f = open('diagnostics/diagnostics.txt', 'w')
                init_txt=['Picard_it', '\t', 'passed_time', '\t', 'step_size', '\t', 'H', '\t', 'S', '\t', 'div', '\t', '\t', 'JxH', '\t', 'runtime', '\n']
                f.writelines(init_txt)
                f.close()

        # Start
        if not restart:
            B_0_h = b
        else:
            V = self._derham_h.V2.vector_space
            v = V.zeros()
            B_0_h = self._parallel_data_transfer(b, v, V)

        if not restart:
            if save_b_field_at is not None:
                save_t = 0.
                save_ts = 0
                add_snapshot(Om, B_0_h, self._derham_h.V2, save_t, save_ts)

        if not restart:
            hamil = self._hamiltonian(B_0_h)
            entro = self._entropy(B_0_h)
            div = self._divergence(B_0_h)

        # Initialize storage for the solution
        b_history = [B_0_h]

        # Advance in time. Keep track of the passed time t and the steps taken n.
        if restart:
            t = t_re
            n = n_re
            n_b = 0
        else:
            t = 0
            n = 0
            n_b = 0

        if self._mpi_rank == 0:
            global_start = time.time()
            if not restart:
                f = open('diagnostics/diagnostics.txt', 'a')
                txt_append = [str(n), '\t', str(t), '\t', str(dt), '\t', str(hamil), '\t', str(entro), '\t', str(div), '\t', 'x', '\t', 'x', '\n']
                f.writelines(txt_append)
                f.close()
        
        while t < maxtime:

            start = time.time()

            # initialize err required for the picard-while loop
            err = 2*err_tol

            # B_h^p <- B_h^n as well as count picard iterations. 
            step = 0
            self._comm.Barrier()
            B_p_h = b_history[n_b]

            if self._mpi_rank == 0:
                p_start_null = 0

            while (err > err_tol) and (step < maxiter):

                self._comm.Barrier()
                if self._mpi_rank == 0:
                    p_start = time.time()
                B_p1_h, H_p_h = self._picard_iteration(B_p_h, dt, b_history[n_b], step)
                self._comm.Barrier()
                if self._mpi_rank == 0:
                    p_stop = time.time()
                    p_start_null += ( p_stop - p_start )

                #comm.Barrier()

                # 6. Compute the new L^{\infty} norm of B_h^{p+1} - B_h^p (right now: L2-norm)
                err = self._get_error(B_p_h, B_p1_h)
                if self._mpi_rank == 0:
                    print(f"L2-Picard error in iteration {step+1}: {err}")

                # 7. B_h^{p+1} <- B_h^p
                B_p_h = B_p1_h
                B_p_h.copy(out=self._x0_s) # necessary, because the system matrix changes every picard step

                # 8. Keep track of the Picard iterations passed
                step += 1

            # Stop if no convergence was achieved after maxiter picard iteraions
            if (step >= maxiter) and (err > err_tol):
                raise ValueError(f"The picard loop did not converge in {maxiter} steps.")
            
            # Given B_h^p, compute B_h^{n+1}, E_h^{n+1/2}, J_h^{n+1/2} and H_h^{n+1/2} as well as Hamiltonian and Entropy
            B_n1_h = 2 * B_p_h - b_history[n_b]
            #E_n12_h = None
            J_n12_h = self._get_j(B_p_h)
            H_n12_h = H_p_h
            if ((n+1) % self._hamiltonian_at) == 0:
                if self._mpi_rank == 0:
                    h_start = time.time()
                hamil = self._hamiltonian(B_n1_h)
                if self._mpi_rank == 0:
                    h_stop = time.time()
            entro = self._entropy(B_n1_h)
            div = self._divergence(B_n1_h)
            jch = self._get_jxh(J_n12_h, H_n12_h)

            # Add to the solution storage
            b_history.append(B_n1_h)

            # Keep track of the passed time, the amount of timesteps taken and update dt
            t += dt
            n += 1
            n_b += 1
            dt = self._get_dt(dt, step)

            # save the b_field if:
            if save_b_field_at is not None:
                if t > save_time:
                    save_t = t
                    if restart:
                        save_ts = save_count
                    else:
                        save_ts = save_count + 1
                    add_snapshot(Om, B_n1_h, self._derham_h.V2, save_t, save_ts)
                    if save_count < (save_len - 1):
                        save_count += 1
                        save_time = save_b_field_at[save_count]
                    else:
                        save_time = -1

            if self._mpi_rank == 0:
                if restart:
                    f = open(restart_diagnostics, 'a')
                else:
                    f = open('diagnostics/diagnostics.txt', 'a')
                global_stop = time.time()
                if restart:
                    runtime = runtime_re + ( global_stop - global_start )
                else:
                    runtime = global_stop - global_start
                if (n % self._hamiltonian_at) == 0:
                    txt_append = [str(n), '\t', str(t), '\t', str(dt), '\t', str(hamil), '\t', str(entro), '\t', str(div), '\t', str(jch), '\t', str(runtime), '\n']
                else:
                    txt_append = [str(n), '\t', str(t), '\t', str(dt), '\t', 'x', '\t', str(entro), '\t', str(div), '\t', str(jch), '\t', str(runtime), '\n']
                f.writelines(txt_append)
                f.close()

            stop = time.time()

            if self._mpi_rank == 0:
                print()
                print(f"Summary Step {n}:")
                print()
                if (n % self._hamiltonian_at) == 0:
                    print(f"{step} Picard iterations, {stop-start} seconds (Picard: {p_start_null} seconds, Hamiltonian: {h_stop-h_start} seconds)")
                else:
                    print(f"{step} Picard iterations, {stop-start} seconds (Picard: {p_start_null} seconds)")
                print(f"t = {t}, dt = {dt}")
                print()

    def _parallel_data_transfer(self, b, v, V):

        localslice0 = tuple([slice(s, e+1) for s, e in zip(V[0].starts, V[0].ends)])
        v[0][localslice0] = b[0][localslice0]
        localslice1 = tuple([slice(s, e+1) for s, e in zip(V[1].starts, V[1].ends)])
        v[1][localslice1] = b[1][localslice1]
        localslice2 = tuple([slice(s, e+1) for s, e in zip(V[2].starts, V[2].ends)])
        v[2][localslice2] = b[2][localslice2]

        return v

    def _get_restart_data(self, restart_diagnostics):

        f = open(restart_diagnostics, 'r')
        lines = f.readlines()
        last_line = lines[-1].split()
        n = int(last_line[0])
        t = float(last_line[1])
        dt = float(last_line[2])
        runtime = float(last_line[7])

        return n, t, dt, runtime

    def _divergence(self, b):

        D = self._D
        M3 = self._M3

        div = D @ b
        out = np.sqrt( ( M3 @ div ).dot( div ) )
        return out

    def _get_j(self, B_p_h):

        Jmat = self._Jmat
        out = Jmat @ B_p_h

        return out

    def _get_dt(self, dt, step):
        
        N_max = self._N_max
        N_min = self._N_min
        f_update = self._f_update
        dt_max = self._dt_max

        if step <= N_min and dt < dt_max:
            dt *= f_update
        elif step > N_max:
            dt /= f_update

        return dt

    def _vector_potential(self, b):
        """Computes the coefficient vector a of A in V_h^1 s.th. curl A = B, weak-div A = 0. b being the coefficients of B."""

        M2 = self._M2
        Ct = self._Ct
        V0 = self._V0
        V10 = self._V10

        VPM_inverse = self._VPM_inverse

        rhs0 = Ct @ (M2 @ b)
        rhs1 = V0.zeros()
        rhs = BlockVector(V10, (rhs0, rhs1))

        out = VPM_inverse @ rhs

        if self._mpi_rank == 0:
            print(f"{VPM_inverse.get_info()} <- Vector potential")

        a = out[0]

        return a
    
    def _entropy(self, b):
        """Computes the discrete entropy S of B, defined as 0.5 * \int_{\Omega} |B|^2 \mathrm{d}x. b being the coefficient vector of B."""

        M2 = self._M2

        out = ( M2 @ b ).dot( b )
        out *= 0.5

        return out
    
    def _magnetic_energy(self, b):
        """Computes the discrete magnetic energy W of B, defined as 1/(8*pi) * \int_{\Omega} |B|^2 \mathrm{d}x. b being the coefficient vector of B."""
        
        out = self._entropy(b)
        out *= 1/(4*np.pi)
        
        return out
    
    def _hamiltonian(self, b):
        """
        Computes the discrete hamiltonian H of B, defined as \int_{\Omega} A\cdot B \mathrm{d}x. b being the coefficient vector of B, 
        A the vector potential of B and a the coefficients of A. If a is not provided, it will be computed from b.
        """

        M12 = self._M12
        a = self._vector_potential(b)

        out = ( M12 @ b ).dot( a )

        return out
    
    def _magnetic_helicity(self, b, a=None):
        return self._hamiltonian(b, a)

    def _compute_H_p_h(self, B_p_h):
        """Computes the projection of B_h^p into V_h^1, called H_h^p."""

        M12 = self._M12
        M1_solver = self._M1_solver

        rhs = M12 @ B_p_h
        H_p_h = M1_solver @ rhs

        return H_p_h
    
    def _picard_iteration(self, B_p_h, dt, B_n_h, step):

        start_total = time.time()

        N_min = self._N_min
        
        p_solver = self._picard_solver
        p_tol = self._picard_tol
        p_pc = self._picard_pc
        p_maxiter = self._picard_maxiter
        x0_s = self._x0_s

        if step < int(N_min/2):
            p_maxiter /= 10
        elif ( step >= int(N_min/2) ) and ( step < N_min ):
            p_maxiter /= 5
        elif ( step >= N_min ) and ( step < N_min + 3 ):
            p_maxiter /= 2
        
        p_maxiter = int(p_maxiter)

        M1_solver = self._M1_solver

        M2 = self._M2
        C = self._C
        Ct = self._Ct

        # 1. H_h^p <- P_1( B_h^p )
        H_p_h = self._compute_H_p_h(B_p_h)

        # 2. Compute Q(H_h^p)
        Q_H_p_h = self._get_Q(H_p_h)

        # 3. Compute A using the two above objects
        A = M2 @ C @ M1_solver @ Q_H_p_h @ M1_solver @ Ct @ M2

        # 4. Compute S, S_inv and the rhs-vector M2 @ B_h^n
        S = A
        S *= dt/2
        S += M2

        S_inv = inverse(S, solver=p_solver, x0=x0_s, tol=p_tol, maxiter=p_maxiter)

        rhs = M2 @ B_n_h

        #self._comm.Barrier()

        start = time.time()
        B_p1_h = S_inv @ rhs
        stop = time.time()
        if self._mpi_rank == 0:
            print(f"{S_inv.get_info()} in {stop-start} seconds, total {stop-start_total} seconds")

        return B_p1_h, H_p_h
    
    def _get_jxh(self, j, h):

        jxh_h = self._jxh_h
        J = FemField(self._derham_h.V1, j)
        H = FemField(self._derham_h.V1, h)
        JxH = jxh_h.assemble(J=J, H=H)
        out = ( JxH @ h ).dot( j )

        return out

    def _get_Q(self, h):

        q_h = self._q_h
        H = FemField(self._derham_h.V1, h)
        Q = q_h.assemble(h1=H)

        return Q

    def _get_error(self, B_p_h, B_p1_h):
        """For now: L2-error. Eventually L\infty-error as proposed by thesis."""

        M2 = self._M2
        d = B_p1_h - B_p_h
        out = M2 @ d
        out = d.dot(out)
        out = np.sqrt(out)

        return out

    @property
    def derham_h(self):
        return self._derham_h

    @property
    def P1(self):
        return self._P1
    
    @property
    def P2(self):
        return self._P2

    @property
    def C(self):
        return self._C
    
    @property
    def Ct(self):
        return self._Ct
    
    @property
    def G(self):
        return self._G
    
    @property
    def Gt(self):
        return self._Gt
    
    @property
    def M1(self):
        return self._M1
    
    @property
    def M2(self):
        return self._M2

    @property
    def V0(self):
        return self._V0
    
    @property
    def V1(self):
        return self._V1
    
    @property
    def V2(self):
        return self._V2
    
    @property
    def comm(self):
        return self._comm
